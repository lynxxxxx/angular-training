import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { JokeRewiewComponent } from "./component/joke-rewiew/joke-rewiew.component";
import { AuthGuard } from "./guards/auth.guard";
import { HomePage } from "./pages/home/home.page";
import { JokeRewiewPage } from "./pages/joke-rewiew/joke-rewiew.page";
import { JokePage } from "./pages/joke/joke.page";
import { LoginPage } from "./pages/login/login.page";


const routes: Routes = [
    
    {
        path: '',
         pathMatch:'full', 
         redirectTo: '/home'
    },

    {
        path: "login",
        component: LoginPage
    },
    {
        path: "home",
        component: HomePage,
     
    },
    {
        path: "joke",
        component: JokePage,
        canActivate: [AuthGuard]
    },
    { path: 'review', component: JokeRewiewPage, canActivate:[AuthGuard] }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], //import a module
    exports: [
        RouterModule
    ]  //export a module and it's features
    })

    export class AppRoutingModele {}
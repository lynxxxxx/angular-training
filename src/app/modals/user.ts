

export type User = {
    userName:string,
    email:string,
    firstName: string,
    lastName: string,
    profilePicUrl: string

}
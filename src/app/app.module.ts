import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeContentComponent } from './component/home-contents/home-content.component';
import { JokeListItemComponent } from './component/joke-list-item/joke-list-item.component';
import { JokeListComponent } from './component/joke-list/joke-list.component';
import { LoginFormComponent } from './component/login-form/login-form.component';

import { HomePage } from './pages/home/home.page';
import { LoginPage } from './pages/login/login.page';
import { AppRoutingModele } from './app.router.modele';
import { JokePage } from './pages/joke/joke.page';
import { JokeRewiewComponent } from './component/joke-rewiew/joke-rewiew.component';
import { JokeRewiewPage } from './pages/joke-rewiew/joke-rewiew.page';


@NgModule({
  declarations: [
    AppComponent,
    HomeContentComponent,
    JokeListItemComponent,
    JokeListComponent,
    LoginFormComponent,
    HomePage,
    LoginPage,
    JokePage,
    JokeRewiewComponent,
    JokeRewiewPage

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModele
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

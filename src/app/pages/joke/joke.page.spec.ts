import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokePage } from './joke.page';

describe('JokePage', () => {
  let component: JokePage;
  let fixture: ComponentFixture<JokePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokePage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JokePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

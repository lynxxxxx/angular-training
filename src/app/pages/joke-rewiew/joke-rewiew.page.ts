import { Component, OnInit } from '@angular/core';
import { Joke } from 'src/app/modals/joke';
import { User } from 'src/app/modals/user';
import { JokeService } from 'src/app/services/joke.service';
import keycloak, { KeycloakTokenParsedExtended } from 'src/keycloak';

@Component({
  selector: 'app-joke-rewiew',
  templateUrl: './joke-rewiew.page.html',
  styleUrls: ['./joke-rewiew.page.css']
})
export class JokeRewiewPage implements OnInit {

  constructor(private readonly jokesService:JokeService) { }

  currentUser:User | any = {  userName:'string',
  email:'string',
  firstName:'string',
  lastName:'string',
  profilePicUrl:'string'}



  get jokes(): Joke[] {
    return this.jokesService.jokes
  }

  

  ngOnInit(): void {
    this.currentUser.firstName = keycloak.tokenParsed?.name 
    this.currentUser.email = keycloak.tokenParsed?.email 


  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeRewiewPage } from './joke-rewiew.page';

describe('JokeRewiewPage', () => {
  let component: JokeRewiewPage;
  let fixture: ComponentFixture<JokeRewiewPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokeRewiewPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JokeRewiewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

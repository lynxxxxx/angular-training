import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {

  constructor(private readonly http: HttpClient) { }

  ngOnInit(): void {
  }

  loginWithKeycloak(): void {
    keycloak.login()
  }


  logOutWithKeycloak(): void {
    keycloak.logout()
    
  }
  
  showToken() {
    console.log(keycloak.token);
    console.log(keycloak.tokenParsed);
    
    
  }

  UseToken(){

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${keycloak.token}`,
    // ' Access-Control-Allow-Origin' : 'http://localhost:4200' *
    })

    this.http.get('https://acc-products-fake.herokuapp.com/api/products',{headers:headers})
    .subscribe(
      {
        next:(reponse)=>{
          console.log(reponse)
        },
        error:(error)=>{console.log(error)}
      }
    )
  }

}

//   "realm": "Rinat",
//   "auth-server-url": "http://localhost:8080/",
//   "ssl-required": "external",
//   "resource": "angular-client",
//   "public-client": true,
//   "confidential-port": 0
// }

import { Component,  EventEmitter,  Input, OnInit, Output } from '@angular/core';
import { Joke } from 'src/app/modals/joke';
import { JokeResponse } from 'src/app/modals/jokeResponse';



@Component({
  selector: 'app-joke-list-item',
  templateUrl: './joke-list-item.component.html',
  styleUrls: ['./joke-list-item.component.css']
})
export class JokeListItemComponent {

@Input() currentJoke?: Joke = {setup:'',delivery:'',id:0,rating:'😂'}
 
@Output() funny: EventEmitter<any> = new EventEmitter();
  constructor() { }


  triggerFunnyEvent(): void {
    console.log('ready');
    this.funny.emit('hello')
    
  }

}

import { Component } from "@angular/core";

@Component({
    selector:'app-home-content',
    templateUrl:'./home-content.component.html',
    styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent{
    constructor() {}
    log:boolean = false
    message: string = 'hello NG'
    dailyTodos:string [] = ['riko, borz, shora, chopa']
    picUrl: string = 'https://wallsdesk.com/wp-content/uploads/2018/03/Pictures-of-Elk.jpg'
}
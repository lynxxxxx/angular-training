import { HttpClient } from '@angular/common/http';
import { Component,  Input,  OnInit } from '@angular/core';
import { Joke } from 'src/app/modals/joke';
import { User } from 'src/app/modals/user';
import { JokeService } from 'src/app/services/joke.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['./joke-list.component.css']
})
export class JokeListComponent implements OnInit {

  //joke will get passed down to child
  currentJoke: Joke = {
    setup: "some joke",
    delivery: "hahddddddddda",
    id: 6,
    rating:'😐'
  }


  jokeIndexer: number = 0



  constructor(
    private readonly http: HttpClient,
    private readonly loginService: LoginService,
    private readonly jokeService: JokeService
  ) { }


  get currentUser(): User {
    return this.loginService.currentUser
  }
  get jokes(): Joke[] {
    return this.jokeService.jokes
  }

  ngOnInit(): void {
    this.jokeService.getJoke()
    this.currentJoke = this.jokes[0]
    
  }

  handleNextJoke(): void {
    this.jokeService.getJoke()
    this.jokeIndexer++
   
  }

  // handleEventFromChild(message: any){
  // console.log("i hear you joke" + message);
  
  // }

  handleJokeUpVote(): void {
   this.jokeService.rateJoke('😂')
   
  }
  handleJokeDownVote():void {
   
    this.jokeService.rateJoke('😐')


  }


}

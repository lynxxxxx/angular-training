import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeRewiewComponent } from './joke-rewiew.component';

describe('JokeRewiewComponent', () => {
  let component: JokeRewiewComponent;
  let fixture: ComponentFixture<JokeRewiewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokeRewiewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JokeRewiewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

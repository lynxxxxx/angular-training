import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/modals/user';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(
    private readonly http: HttpClient,
    private readonly loginService: LoginService,
    private readonly router: Router
    ){ }

  ngOnInit(): void {
  }

  get currentUser(): User {
    return this.loginService.currentUser
  }

  // login(form: NgForm): void {
  //   this.loginService.loginAttempt()
  //   this.router.navigate(['joke'])
  // }

}

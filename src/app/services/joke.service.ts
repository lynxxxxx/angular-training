import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Joke } from '../modals/joke';
import { JokeResponse } from '../modals/jokeResponse';

@Injectable({
  providedIn: 'root'
})
export class JokeService {

  private _jokes: Joke[] = [];

  private JOKES_URL: string = 'https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit'

  constructor(private readonly http: HttpClient) { }

  get jokes(): Joke[] {
    return this._jokes
  }
  
  rateJoke(rating:string){
    this._jokes[this._jokes.length-1].rating = rating
  }


  getJoke(): void {
     this.http.get<JokeResponse>(this.JOKES_URL)
     .pipe(
      map(response => {
        let parseJoke: Joke = {
          setup: response.setup,
          delivery: response.delivery,
          id: response.id,
          rating: '🤡'
        }
        return parseJoke
      })
     )
     .subscribe({
      next: response => {this._jokes.push(response);
      },
      error: error => {
        console.log(error);
      }
     })
  }
}

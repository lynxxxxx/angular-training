import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first, map } from 'rxjs';
import { User } from '../modals/user';
import { UserResponse } from '../modals/userResponse';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _currentUser: User = {
    email:"riko.com",
    firstName: "borz",
    userName: "ilbirs",
    lastName: "borzov",
    profilePicUrl: ""
  }

  get currentUser(): User {
    return this._currentUser
  }

  private LOGIN_URL: string =  'https://randomuser.me/api/'

  constructor(private readonly http: HttpClient) { }

  loginAttempt(): void {

    this.http.get<UserResponse>(this.LOGIN_URL)
    .pipe(
      map(response => {
        let parsedUser: User = {
          email: response.results[0].email,
          firstName: response.results[0].name.first,
          lastName: response.results[0].name.last,
          profilePicUrl: response.results[0].picture.medium,
          userName: response.results[0].login.username,
        }
        return parsedUser
      }),
    )
    .subscribe({
      next: (user) => {
        this._currentUser = user
      },
      error: (error) => {
        console.log(error);
        
      }
    })

  }
}
